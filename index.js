const express = require("express");

const mongoose = require("mongoose");

const cors = require("cors");

//Allow us to access the routes 

const userRoutes = require("./routes/userRoutes");

const productRoutes = require("./routes/productRoutes");


const port =  process.env.PORT || 4000;


const app = express();

//Connect to our MongoDb Database
mongoose.connect("mongodb+srv://admin:admin123@batch204-ericjohnmagang.yrt1nur.mongodb.net/Capstone2?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
});
let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));
//Allow resources to access our back end application
app.use(cors());
app.use(express.json());

//localhost:4000/users
app.use("/users", userRoutes)

app.use("/products", productRoutes);


app.listen(port, ()=> {
	console.log(`API is now online on port ${port}`);
});

