const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");

//Route for checking if the user's email already exist in the database
router.post("/CheckEmail", (req, res)=> {

	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});

//User Authentication
router.post("/login", (req,res)=> {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});



router.post("/details", (req, res) =>{

		const userData = auth.decode(req.headers.authorization);

		console.log(userData);
		console.log(req.headers.authorization);

	userController.getProfile({userData}).then(resultFromController => res.send(resultFromController))
});

//s41 discussion

//Route to enroll user to a course
	router.post("/enroll", auth.verify, (req, res) => {

		let data = {
			userId: req.body.userId,
			courseId: req.body.courseId
		}

		userController.enroll(data).then(resultFromController => res.send (
			resultFromController));



	});





module.exports = router;
