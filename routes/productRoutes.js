const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require('../auth.js');

//Rout for creating  a course
// router.post("/", auth.verify, (req, res)=> {
// 	courseController.addCourse(req.body). then(resultFromController => res.send(resultFromController));
// });





// route for creating a product
router.post("/", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(
			resultFromController));
	} else {
		return res.send("User is not  Admin");
	}
});
//---------------------------
//Route for retrieving all the product
router.get("/all", (req, res)=> {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});




//Route for creating a product
router.post("/", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin);

	if(isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		
		res.send(false);
	}
	

});

router.post("/", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(
			resultFromController));
	} else {
		return res.send("User is not  Admin");
	}
});

//Route for retrieving all the active products
router.get("/", (req, res)=> {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});



//Routes for retrieving specific product

router.get("/:productId", (req, res)=>{
	console.log(req.params)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})


//Route for Updating a PRODUCT
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}

});




//Route for archiving product

router.put("/:productId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.archiveProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));

});










module.exports = router;