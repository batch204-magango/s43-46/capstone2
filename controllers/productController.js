const Product = require("../models/Product");
const User = require("../models/User");
const productController = require("../controllers/productController");


//Create a New Product
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	return newProduct.save().then( (product, error) => {

		if(error) {

			return false

		} else {

			return true
		}

	});
};




//Retrieve all product
module.exports.getAllProduct =()=> {
	return Product.find({}).then(result => {
		return result;
	})
}


//controller for retrieving all active product
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then (result => {
		return result;
	})
}

//Retrieving a specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}


//Updating Course
// module.exports.updateCourse = (reqParams, reqBody, data) => {


// 	if(data.isAdmin === true) {
// 		let updatedCourse = {
// 			name:reqBody.name,
// 			description: reqBody.description,
// 			price:reqBody.price
// 		};
// 		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
// 			(course, error)=> {
// 				if(error) {
// 					return false
// 				} else {
// 					return true
// 				}
// 			})
// 	} else {
// 		return false
// 	}



// }




module.exports.updateProduct = (reqParams, reqBody, data) => {

    return User.findById(data.id).then(result => {
        console.log(result)

        if(result.isAdmin === true) {

            // Specify the fields/properties of the document to be updated
            let updatedProduct = {
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            };

            // Syntax:
                        // findByIdAndUpdate(document ID, updatesToBeApplied)
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

                // product not updated
                if(error) {

                    return false

                // product updated successfully    
                } else {

                    return true
                }
            })

        } else {

            return false
        }



    })


}

	//Archiving product


module.exports.archiveProduct = (reqParams, reqBody, data) => {

	if(data.isAdmin === true){
		let archivedProduct = {
			isActive: reqBody.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
			if(error){
				return false
			}
			else{
				return true
			}
		})
	}
	else{
		return false
	}
}
